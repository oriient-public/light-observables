//   
// Copyright (C) Oriient New Media Ltd - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Created by Michael Krutoyarskiy, 2021



import Foundation

// Internal class to hold a reference to a subscription
final class PropertyObserver<T>: Hashable {
    let id: UUID
    let onEvent: (T) -> Void
    let queue: DispatchQueue

    init(
        id: UUID,
        queue: DispatchQueue,
        onEvent: @escaping (T) -> Void
    ) {
        self.id = id
        self.onEvent = onEvent
        self.queue = queue
    }

    func hash(into hasher: inout Hasher) {
        id.hash(into: &hasher)
    }

    static func == (lhs: PropertyObserver<T>, rhs: PropertyObserver<T>) -> Bool { lhs.id == rhs.id }
}
