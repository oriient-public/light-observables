//
// Copyright (C) Oriient New Media Ltd - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Created by Michael Krutoyarskiy, 2021



import Foundation

/// This class represents an observable property.
public final class ObservableProperty<T> {
    private var observers = Set<PropertyObserver<T>>()
    private let lock = NSRecursiveLock()

    /// The most relevant value of this property.
    public var value: T {
        self.lock.lock()
        defer { self.lock.unlock() }
        return _value
    }

    private var _value: T

    fileprivate var onObserversCountUpdated: ((Int) -> Void)?

    /// Start observing the updates.
    /// Important: You need to retain the `ObservationToken` returned by this method to keep receiving the updates.
    /// Use `ObservationTokensContainer` to conviniently store multiple tokens.
    public func observe(
        on queue: DispatchQueue = .global(qos: .default),
        _ onEvent: @escaping (T) -> Void
    ) -> ObservationToken {
        self.lock.lock()
        defer { self.lock.unlock() }

        let id = UUID()
        let observer = PropertyObserver(id: id, queue: queue, onEvent: onEvent)

        self.observers.insert(observer)
        self.onObserversCountUpdated?(self.observers.count)

        return ObservationToken(id: id) { [weak self] in
            self?.onObserverDisposed(observer)
        }
    }

    private func onObserverDisposed(_ observer: PropertyObserver<T>) {
        self.lock.lock()
        defer { self.lock.unlock() }

        self.observers.remove(observer)
        self.onObserversCountUpdated?(self.observers.count)
    }

    fileprivate func update(_ value: T) {
        self.lock.lock()
        defer { self.lock.unlock() }

        self._value = value
        self.observers.forEach { observer in
            observer.queue.async {
                observer.onEvent(value)
            }
        }
    }

    init(_ defaultValue: T) {
        self.lock.lock()
        defer { self.lock.unlock() }
        self._value = defaultValue
    }
}

/// This class represents a mutable observable property.
@propertyWrapper
public final class MutableObservableProperty<T> {
    public let wrappedValue: ObservableProperty<T>

    /// Handle the observers count updates
    public var onObserversCountUpdated: ((Int) -> Void)? {
        get {
            self.wrappedValue.onObserversCountUpdated
        }
        set {
            self.wrappedValue.onObserversCountUpdated = newValue
        }
    }

    /// Initialize a new mutable property
    public init(_ defaultValue: T) {
        self.wrappedValue = ObservableProperty(defaultValue)
    }

    /// Update the underlying value and notify the observers
    public func update(_ value: T) {
        wrappedValue.update(value)
    }
}
