//   
// Copyright (C) Oriient New Media Ltd - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Created by Michael Krutoyarskiy, 2021



import Foundation

/// Convinient containter to store the observation tokens.
public final class ObservationTokensContainer {
    private var tokens = Set<ObservationToken>()

    /// Stores this `ObservationToken` in the container.
    public func store(_ token: ObservationToken) {
        tokens.insert(token)
    }

    /// Disposes all the tokens from this container. It will stop all the relevant updates.
    public func disposeAll() {
        tokens = Set<ObservationToken>()
    }

    /// Dispose the token and stop the relevant updates.
    public func disposeToken(_ token: ObservationToken) {
        tokens.remove(token)?.dispose()
    }

    /// Initialize a new container
    public init() { }
}

public extension ObservationToken {
    /// Stores this `ObservationToken` in the `ObservationTokensContainer`.
    func store(in container: ObservationTokensContainer) { container.store(self) }
}
