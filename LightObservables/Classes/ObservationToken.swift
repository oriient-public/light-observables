//   
// Copyright (C) Oriient New Media Ltd - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Created by Michael Krutoyarskiy, 2021



import Foundation

/// This token usually returned when someone starts observing an events stream.
/// It should be retained to continue the receiving the updates.
public final class ObservationToken: Hashable {
    private let id: UUID
    private let onDisposed: () -> Void
    private var isDisposed: Bool = false

    internal init(
        id: UUID,
        onDisposed: @escaping () -> Void
    ) {
        self.id = id
        self.onDisposed = onDisposed
    }

    internal func dispose() {
        if isDisposed { return }
        isDisposed = true
        onDisposed()
    }

    deinit {
        dispose()
    }

    public func hash(into hasher: inout Hasher) {
        id.hash(into: &hasher)
    }

    public static func == (lhs: ObservationToken, rhs: ObservationToken) -> Bool { lhs.id == rhs.id }
}
