//
// Copyright (C) Oriient New Media Ltd - All Rights Reserved
// Unauthorized copying of this file, via any medium is strictly prohibited
// Proprietary and confidential
// Created by Michael Krutoyarskiy, 2021



import Foundation

/// This class represents an observable events stream.
public final class ObservableEvent<T> {
    private var observers = Set<PropertyObserver<T>>()
    private let lock = NSRecursiveLock()

    fileprivate func update(_ value: T) {
        self.lock.lock()
        defer { self.lock.unlock() }
        observers.forEach { observer in
            observer.queue.async {
                observer.onEvent(value)
            }
        }
    }

    fileprivate var onObserversCountUpdated: ((Int) -> Void)?

    /// Start observing the updates.
    /// Important: You need to retain the `ObservationToken` returned by this method to keep receiving the updates.
    /// Use `ObservationTokensContainer` to conviniently store multiple tokens.
    public func observe(
        on queue: DispatchQueue = .global(qos: .default),
        _ onEvent: @escaping (T) -> Void
    ) -> ObservationToken {
        self.lock.lock()
        defer { self.lock.unlock() }

        let id = UUID()
        let observer = PropertyObserver(id: id, queue: queue, onEvent: onEvent)

        self.observers.insert(observer)
        self.onObserversCountUpdated?(self.observers.count)

        return ObservationToken(id: id) { [weak self] in
            self?.disposeObserver(observer)
        }
    }

    private func disposeObserver(_ observer: PropertyObserver<T>) {
        self.lock.lock()
        defer { self.lock.unlock() }
        self.observers.remove(observer)
        self.onObserversCountUpdated?(self.observers.count)
    }
}

/// This class represents a mutable observable event.
@propertyWrapper
public final class MutableObservableEvent<T> {
    public let wrappedValue = ObservableEvent<T>()

    /// Handle the observers count updates
    public var onObserversCountUpdated: ((Int) -> Void)? {
        get {
            self.wrappedValue.onObserversCountUpdated
        }
        set {
            self.wrappedValue.onObserversCountUpdated = newValue
        }
    }

    /// Update the underlying value and notify the observers
    public func update(_ value: T) {
        wrappedValue.update(value)
    }

    /// Initialize a new mutable event
    public init() {

    }
}

