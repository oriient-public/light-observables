# LightObservables

[![Version](https://img.shields.io/cocoapods/v/LightObservables.svg?style=flat)](https://cocoapods.org/pods/LightObservables)
[![License](https://img.shields.io/cocoapods/l/LightObservables.svg?style=flat)](https://cocoapods.org/pods/LightObservables)
[![Platform](https://img.shields.io/cocoapods/p/LightObservables.svg?style=flat)](https://cocoapods.org/pods/LightObservables)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LightObservables is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LightObservables'
```

## Author

Michael, mkrutoyarskiy@gmail.com

## License

LightObservables is available under the MIT license. See the LICENSE file for more info.
