#
# Be sure to run `pod lib lint LightObservables.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LightObservables'
  s.version          = '0.2.0'
  s.summary          = 'A simple and lightweight Observable pattern implementation with no third-party dependencies.'
  s.description      = <<-DESC
  LightObservables is a simple and lightweight Observable pattern implementation with no third-party dependencies.
  With LightObservables you can easily set up an API which provides access to an events stream.
  The framework is designed to be minimal meanwhile convenient.
                         DESC
  s.homepage         = 'https://gitlab.com/oriient-public/light-observables'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Michael Krutoyarskiy' => 'michaelk@oriient.me' }
  s.source           = { :git => 'https://gitlab.com/oriient-public/light-observables.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'LightObservables/Classes/**/*'
  s.swift_version = '5.0'
end
