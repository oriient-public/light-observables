//
//  EventsSource.swift
//  LightObservables_Example
//
//  Created by Michael Krutoyarskiy on 07/04/2021.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import Foundation
import LightObservables


class EventsSource {

    @MutableObservableProperty("Initial value")
    var property: ObservableProperty<String>

    @MutableObservableEvent
    var eventsStream: ObservableEvent<String>

    private var timer: Timer?

    init() {
        self._property.onObserversCountUpdated = { newCount in
            print("Property now is observed by \(newCount) observers")
        }

        self._eventsStream.onObserversCountUpdated = { newCount in
            print("Event now is observed by \(newCount) observers")
        }
    }

    func startUpdates() {
        if timer != nil {
            return
        }

        timer = Timer.scheduledTimer(withTimeInterval: 1.5, repeats: true, block: { [weak self] (_) in
            self?._property.update("Property Update \(Date())")
            self?._eventsStream.update("New Event \(Date())")
        })
    }

    func stopUpdates() {
        timer?.invalidate()
        timer = nil
    }
}
