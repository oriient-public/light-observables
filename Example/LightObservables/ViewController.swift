//
//  ViewController.swift
//  LightObservables
//
//  Created by Michael on 04/07/2021.
//  Copyright (c) 2021 Michael. All rights reserved.
//

import UIKit
import LightObservables


class ViewController: UIViewController {

    // Retains multiple events subscriptions
    // The subscriptions will be released with the container
    private let container = ObservationTokensContainer()

    // Example events source
    private let eventsSource = EventsSource()

    // Labels for the updates
    @IBOutlet weak var propertyLabel: UILabel!
    @IBOutlet weak var eventLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        propertyLabel.text = "Initial Property Value:\n'\(eventsSource.property.value)'"

        // Observe using the default queue
        eventsSource.property.observe { newValue in
            print("Property Value Updated: '\(newValue)'")
        }.store(in: container)

        // Observe using the main queue to update the UI
        eventsSource.property.observe(on: .main, { [weak self] (newValue) in
            guard let strong = self else {
                return
            }
            strong.propertyLabel.text = "Property Value:\n'\(newValue)'"
        }).store(in: container)

        // Observe using the main queue to update the UI
        eventsSource.eventsStream.observe(on: .main) { [weak self] (newEvent) in
            guard let strong = self else {
                return
            }
            strong.eventLabel.text = "Event Value:\n\(newEvent)"
        }.store(in: container)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        eventsSource.startUpdates()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        eventsSource.stopUpdates()
    }
}

